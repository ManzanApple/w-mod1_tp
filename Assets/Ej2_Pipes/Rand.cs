﻿using System;

public static class Rand
{
    static System.Random _rnd = new System.Random();

    //Uno nuevo cada vez que evaluamos la variable
    public static float uniform { get { return (float)_rnd.NextDouble(); } }
}