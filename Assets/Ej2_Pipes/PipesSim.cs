﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;


/*
 * TODO:
 * - Quitar comentarios rancios como este.
 * - 
 */
public class PipesSim : MonoBehaviour {
	public enum Content {
		Empty,
		Tears,
		Block
	}

	class Node {
		public Pipe pipe;
		public Content content = Content.Empty;
        
        // Lista de vecinos (incluye al nodo del cual viene para poder rellenarlo despues)
        public List<Node> neighbours = new List<Node>();
	};

	public Pipe prefabPipe;
	public int maxWidth = 8, maxHeight = 8;
	[Range(0.0f, 1.0f)] public float branchChance = 0.5f;
	[Range(0.0f, 1.0f)] public float blockChance = 0.1f;
	public bool joinExisting = true;
	public float fillDelaySeconds = 0.2f;
	public int maxPipes = 400;

	public Material materialTears;
	public Material materialEmpty;
	public Material materialBlocked;
	public Material materialError;

    /*
     * VARIABLES AGREGADAS
     */

    // Variables agregadas
    private Node _root;

    // Matrix Recargado de nodos visitados(nodos ya puestos en escena)
    private bool[,] visited;

    // Contador de pipes para cumplir con "maxPipes"
    private int pipesCount = 0;

    // #RowDaBoat (IA 1)
    private int[] xMovementVector = new int[] { 1, 0, -1, 0, 0};
    private int[] yMovementVector = new int[] { 0, 1, 0, -1, 0};

    // Diccionario con las direcciones contrarias
    private Dictionary<Direction, Direction> _direccionContraria = new Dictionary<Direction, Direction>();

    // Lista con todas las direcciones en las que se puede mover un pipe.
    private List<Direction> _direccionesValidas = new List<Direction>();

    // Lista de todos los nodos validos en escena (no incluye nodos con bloqueos)
	private List<Node> pipes = new List<Node>();

    // Booleano para terminar con la recursiva
    private bool endScript;


    //¿Esta la posicion ocupada?
    bool PositionOccupied(int x, int y)
    {
        if((x > 0 && x < maxWidth) && (y > 0 && y < maxHeight)) return visited[x, y];
        return true;
	}


	//Creamos un nodo (esta función ya está completa)
	Node CreateNode(int x, int y) {

        visited[x, y] = true; // Agregue que el mismo nodo diga en que X e Y se crea.

		var n = new Node();
		n.pipe = Instantiate(prefabPipe);
		n.pipe.transform.parent = transform;
		n.pipe.x = x;
		n.pipe.y = y;

		pipes.Add (n); // Se agrega a la lista de pipes del mapa
        return n;
	}


    //Creamos el sistema de cañerias
    Node RecuBuildDFS(int x, int y, Node prev = null, Direction comingFrom = Direction.Left) {
        

        // Checkeo si no esta superando el maximo de pipes que puede crearse
        if (pipesCount >= maxPipes)
        {
            // Aviso que el algoritmo termino
            endScript = true;
            return null;
        }


        // Si pudo pasar el algoritmo sumo uno a la cantidad de pipes actuales.
        pipesCount++;


        // Creamos un nodo y lo posicionamos en la escena
        var node = CreateNode(x, y);


        // Checkeo si el pipe anteior no es nulo (si no es el primer pipe en escena)
        if (prev != null)
        {
            // Conecto el anterior pipe con el nuevo.
            prev.pipe.SetConnection(comingFrom, true);
            // Conecto el nuevo pipe con el anterior.
            node.pipe.SetConnection(_direccionContraria[comingFrom], true);
            // Agrego el pipe anterior a la lista de vecinos (por mas que viva atras sigue siendo un vecino)
            node.neighbours.Add(prev);

            // Tiro un random para ver si el nodo tiene que estar bloqueado. Si lo esta no lo agrega a la lista de pipes
			if (RandomBlock (node)) pipes.Add (node);
        }

        
        // Random para crear nuevas ramas
        var branchs = RandomBranch();
        

        // Hago un for para recorrer la ramas que me salieron.
        for (int i = 0; i < branchs; i++)
        {
            // Obtengo las direcciones en las que se va a poder mover el pipe
            var direccionesPosibles = GetDireccionesPosibles(x, y);

            // Si no quedan direcciones posibles o si el script termino porque llego a su maximo cortamos el ciclo.
            if (direccionesPosibles.Count == 0 || endScript) break;

            // Random range para obtener una direccion de movimiento.
            var rndDireccion = Random.Range(0, direccionesPosibles.Count);
            Direction nuevaDireccion = direccionesPosibles[rndDireccion];

            // Sacamos los nuevos X e Y de los vectores de movimiento.
            int newX = x + xMovementVector[(int)nuevaDireccion];
            int newY = y + yMovementVector[(int)nuevaDireccion];

            // Llamamos a la funcion recursiva para seguir creando nuevos nodos.
            var newNode = RecuBuildDFS(newX, newY, node, _direccionContraria[nuevaDireccion]);

            // Si el nuevo nodo no es nulo lo agregamos a la lista de vecinos del nodo actual.
            if(newNode != null) node.neighbours.Add(newNode);
        }

        return node;
	}

    // Obtiene la cantidad de ramas a dividir el nodo actual
    int RandomBranch()
    {
        var rnd = Rand.uniform % branchChance;

        if (rnd >= 0.66f)
            return 3;
        else if (rnd >= 0.33f)
            return 2;
        else
            return 1;
    }

    List<Direction> GetDireccionesPosibles(int x, int y)
    {
        var aux = new List<Direction>();

        foreach (var dir in _direccionesValidas)
        {
            if (!PositionOccupied(x + xMovementVector[(int)dir], y + yMovementVector[(int)dir])) aux.Add(dir);
        }

        return aux;
    }

	// La lista a procesar por como esta armada evita el nodo root, evita los nodos blockeados.
	Node GetRandomNode(){
        var aux = pipes[Random.Range(0, pipes.Count)];

        pipes.Remove(aux);
        if (aux.content == Content.Block)
        {
            return GetRandomNode();
        }

        return aux;
	}

    bool RandomBlock(Node node) {

        var rndResult = Rand.uniform % blockChance;

        if (blockChance > 0.97f || rndResult > 0.5f)
        {
            node.pipe.material = materialBlocked;
            node.content = Content.Block;
			return true;
        }

		return false;
    }


    void CheckErrors() {
		foreach(Transform xf1 in transform) {
			foreach(Transform xf2 in transform) {
				Pipe p1 = xf1.GetComponent<Pipe>();
				Pipe p2 = xf2.GetComponent<Pipe>();
				if(p1!=null && p2!=null && p1 != p2 && p1.x == p2.x && p1.y == p2.y)
					p1.material = p2.material = materialError;
			}
		}
	}

    // Arrancar (esta función esta completa, pero si desea extiendala)
    void Start() {

        // Agrego las direcciones contrarias
        _direccionContraria.Add(Direction.Right, Direction.Left);
        _direccionContraria.Add(Direction.Left, Direction.Right);
        _direccionContraria.Add(Direction.Top, Direction.Bottom);
        _direccionContraria.Add(Direction.Bottom, Direction.Top);

        // Agrego las direcciones validas
        _direccionesValidas.Add(Direction.Right);
        _direccionesValidas.Add(Direction.Left);
        _direccionesValidas.Add(Direction.Top);
        _direccionesValidas.Add(Direction.Bottom);
        _direccionesValidas.Add(Direction.Center);

        // Creo el array de visitados con los tamaños maximos de los bordes
        visited = new bool[maxWidth, maxHeight];

        // Inicio la recursiva
        _root = RecuBuildDFS(maxWidth/2, maxHeight/2, null, Direction.Left);


        /*
         * Le Profe's Code
         */

		//Rotamos una pizca luego de agregar todos los pipes
		transform.Rotate(0f, 0f, -5f);
		CheckErrors();
	}

	void StartFill() {
        print("Llenando");
        // Inicia la corrutina para llenar los pipes, como parametros recibe el nodo root y un nodo
		this.StartCoroutine(Fill(GetRandomNode(), GetRandomNode()));
	}

    Queue<Node> q = new Queue<Node>();

	IEnumerator Fill(Node node1, Node node2) {

        q.Enqueue(node1);
        q.Enqueue(node2);

        var currentPipes = 0;

        while (q.Count > 0)
        {
            node1 = q.Dequeue();

            if (node1.content == Content.Empty)
            {
                node1.content = Content.Tears;
                node1.pipe.material = materialTears;

                foreach (var child in node1.neighbours)
                {
                    q.Enqueue(child);
                }

                currentPipes++;
                if (currentPipes >= 2)
                {
                    yield return new WaitForSeconds(fillDelaySeconds);
                    currentPipes = 0;
                }
            }
        }
    }
	
	bool _filled = false;
	// Recarga la escena al hacer click/apretar cualquier cosa
	void Update () {
		if(Input.GetMouseButtonDown(0)) {
			//Click izquierdo regenera (carga la escena de nuevo)
			SceneManager.LoadScene("Pipes");
		}
		else if(Input.GetMouseButtonDown(1)) {
			//Click derecho rellena de lagrimas
			if(!_filled) {
				_filled = true;
				StartFill();
			}
		}
	}
}
