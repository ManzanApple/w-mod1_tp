﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;


public class Sudoku : MonoBehaviour
{
	public Cell prefabCell;
	public Canvas canvas;
	public Text feedback;
	public float stepDuration = 0.05f;
	public int difficulty = 40;

	Matrix<Cell> _board;
	Matrix<int> _createdMatrix;
	int _smallSide;
	int _bigSide;

	// matriz de testeo
	private int[,] _currentMat = Tests.validBoards[18];

	// auxiliares

	private List<int> _estandar = new List<int>{1, 2, 3, 4, 5, 6, 7, 8, 9};
	private int[] _testedNums = new int[9];
	private List<int> _currentSecNums = new List<int>();
	private int recuCount = 0;
	private bool _preCheck = false;
	private int[] _lastPos = new int[]{0,0};
	private bool _solved = false;
	private int _currentSec = 0;


	// backtracking
	private List<Matrix<int>> solution = new List<Matrix<int>> ();
	private List<int> _bin = new List<int>(); //numeros testeados
	private List<int[]> _binPos = new List<int[]>();
	//private Matrix<int> _solucion;

	//Retorna true si pudo resolverlo o false si fallo
	bool RecuSolve(Matrix<int> matrix, int x, int y, int protectMaxDepth, List<Matrix<int>> solution, int take = 0)
    {


		Debug.Log ("start"+recuCount+"--- x"+x+" y "+y);
		recuCount++;
				
		Matrix<int> mtx = matrix.Clone();		

		int _y = y;
		int _x = x;

		if (_x >= 9) {
			_x = 0;
			_y++;
			if (_y >= 9)
				return _solved;
		}

		while(mtx[_x,_y] != 0){
			Debug.Log (mtx[_x,_y]+"--- depth:"+((_x+1)*(_y+1))+" skip"+_x+":"+_y);
			_x++;
			recuCount++;
			if (_x >= 9) {
				_x = 0;
				_y++;
				if (_y >= 9)
					return _solved;
			}
		}

		if (findNum (mtx, _x, _y, take)) {
			solution.Add (mtx.Clone ());
			if (_x == _lastPos [0] && _y == _lastPos [1]) {
				_solved = true;
				solution.Add (mtx.Clone ());
			}

			if (RecuSolve (mtx.Clone (), _x + 1, _y, ((_x + 1) * (_y + 1)), solution)) {				
				Debug.Log ("done: " + recuCount + "--- pasos :" + _bin.Count + "current: " + matrix [_x, _y]+" at : "+_x+" : "+_y );
				return _solved;
			} else {
				mtx [_x, _y] = 0;
				return RecuSolve (mtx.Clone(), _x, _y, ((_x+1)*(_y+1))-1, solution, take + 1);
			}

		} else {
			solution.Add (mtx.Clone());
			return _solved;
		}

	}


	bool findNum(Matrix<int> matrix, int x, int y, int take)
    {	
		_currentSecNums = getSection (matrix, x, y);

		if (_currentSecNums.Count < take)
        {			
			return false;
		}

		int takeAux = take;

		int check = _currentSecNums.Count;

		foreach (int element in _currentSecNums)
        {			
			_bin.Add (element);
			int[] xy = new int[]{ x, y };
			_binPos.Add (xy);
			check--;

			matrix [x, y] = element;
			if (CheckCF (matrix, x, y))
            {
				if (takeAux == 0)
                {
					matrix [x, y] = element;
					_testedNums [_currentSec] = element;
					return true;
				}
                else
                {
					takeAux--;
				}
			}
			if (check == 0) {
				return false;
			}				
		}
		return false;
	
	}

	int[] getLastTrue(Matrix<int> mtx)
    {
		for(int y = 0; y<mtx.Height; y++)
        {
			for(int x = 0; x<mtx.Width; x++)
            {
				if (mtx [x, y] == 0)
                {
					return new int[] {x,y};
				}
			}
		}
		return new int[] {0,0};
	}

	// obtengo los numeros que no aparecen en la seccion.
	List<int> getSection(Matrix<int> matrix, int x, int y)
    {		
		int _y;
		int _x;

		_currentSec = 0;

		if (y < 3)
        {
			_y = 0;
		}
        else if (y < 6)
        {
			_y = 3;
			_currentSec += 3;
		}
        else
        {
			_y = 6;
			_currentSec += 6;
		}

		if (x < 3)
        {
			_x = 0;
		}
        else if (x < 6)
        {
			_x = 3;
			_currentSec += 1;
		}
        else
        {
			_x = 6;
			_currentSec += 2;
		}

		List<int> nums = matrix.GetRange (_x, _y, _x + 2, _y + 2);

		List<int> aux = new List<int> (_estandar);

		foreach (int element in nums)
        {
			if (element != 0)
            {
				aux.Remove (element);
			}			
		}
		return aux;		
	}

	bool CheckCF(Matrix<int> matrix, int i, int j)
    {
		int current = matrix[i,j];

		for (int k = 0; k < matrix.Height; k++)
        {
			if ((k != j ) && matrix [i, k] != 0)
            {

				if (matrix [i, k] == current) { // comparo si existe el numero actual al menos una vez en la columna
					return false;
				}
			}
		}

		for (int l = 0; l < matrix.Width; l++)
        {
			if (l != i && matrix [l, j] != 0)
            {
				if (matrix [l, j] == current) // comparo si existe el numero actual al menos una vez en la fila
					return false;
			}
		}

		return true;
	}

	//*** Este va a ser el core del ejercicio
	//Retorna true si es valido una matriz
	//Si se proporciona una matriz "invalids", se rellenará de booleanos donde encontro los errores
	//(NOTA: El parámetro "invalids" es opcional pero recomendado de implementar para detectar errores visualmente seteando Cell.invalid luego)
	bool CheckValidity(Matrix<int> matrix, Matrix<bool> invalids = null) {

		for (int x = 0; x < matrix.Width; x++)
        {
			for (int y = 0; y < matrix.Height; y++)
            {
				if (matrix[x, y] != 0)
                {
					if (!CheckCF (matrix, x, y))
                    {
						return false;
					}
				}			
			}
		}

		return true;
	}

    bool CheckSec(Matrix<int> matrix, int x, int y)
    {
        int _y;
        int _x;
        bool result = true;

        if (y < 3)
        {
            _y = 0;
        }
        else if (y < 6)
        {
            _y = 3;
        }
        else
        {
            _y = 6;
        }

        if (x < 3)
        {
            _x = 0;
        }
        else if (x < 6)
        {
            _x = 3;
        }
        else
        {
            _x = 6;
        }

        int duplicado = 0;

        int[] aux = matrix.GetRange(_x, _y, _x + 2, _y + 2).ToArray();

        for (int i = 0; i< aux.Length ; i++)
        {
            if (aux[i] == matrix[x, y]) duplicado++;
            if (duplicado > 1) result = false;
        }

        return result;
    }

	void randEstand()
    {
		List<int> aux = new List<int> (_estandar);
		List<int> result = new List<int> ();

		while (aux.Count > 0)
        {
			int rnd = Random.Range (0, aux.Count);
			result.Add (aux [rnd]);
			aux.RemoveAt (rnd);
		}
		_estandar = result;
	}


	// Use this for initialization
	void Start ()
    {
		long mem = System.GC.GetTotalMemory(true);
		feedback.text = string.Format("MEM: {0:f2}MB", mem/(1024f*1024f));

		_smallSide = 3;
		_bigSide = _smallSide * 3;

		for (int i = 0; i < _testedNums.Length; i++)
        {
			_testedNums [i] = 0;
		}

		randEstand();
		CreateEmptyBoard();
		ClearBoard();
	}

	void ClearBoard()
    {
		_createdMatrix = new Matrix<int>(_bigSide, _bigSide);
		SetBoardFromIntMatrix(_createdMatrix, true);

		for(int x = 0;x <_board.Width ;x++){
			for (int y = 0; y < _board.Height; y++) {
				_board [x, y].number = _createdMatrix[x,y];
				if(_board[x, y].number == 0)
				_board[x, y].locked = _board[x, y].invalid = false;
			}
		}
	}

	bool getLast(Matrix<int> mtx)
    {
		for(int x = mtx.Height-1; x > 0 ; x--)
        {
			for(int y = mtx.Width-1; y > 0 ; y--)
            {
				if(mtx[x,y] == 0)
                {
					_lastPos[0] = x;
					_lastPos[1] = y;
					return true;
				}
			}
		}
		return false;
	}


	void SetBoardFromIntMatrix(Matrix<int> mtx, bool lockNonEmpty = false)
    {
		for(int x = 0; x < mtx.Height; x++)
        {
			for(int y = 0; y < mtx.Width; y++)
            {
				mtx[x, y] = _currentMat[x,y];
				_board [x, y].locked = lockNonEmpty;
			}
		}

	}

	void CreateEmptyBoard()
    {
		float spacing = 68f;
		float startX = -spacing * 4f;
		float startY = spacing * 4f;

		_board = new Matrix<Cell>(_bigSide, _bigSide);

		for(int x = 0; x<_board.Width; x++)
        {
			for(int y = 0; y<_board.Height; y++)
            {
				var cell = _board[x, y] = Instantiate(prefabCell);
				cell.transform.SetParent(canvas.transform, false);
				cell.transform.localPosition = new Vector3(startX + x * spacing, startY - y * spacing, 0);
                
			}
		}
	}

	
	IEnumerator ShowSequence(List<Matrix<int>> seq)
    {
		var wait = new WaitForSeconds(stepDuration);

		//...IMPLEMENTAR...

		yield break;
	}

	void FillSudoku ()
    {
		int[] aux = _bin.ToArray ();
		int num = 0;

		foreach (int[] element in _binPos)
        {			
			_board [element[0], element[1]].number = aux[num];
			num++;
		}
	}

	// Update is called once per frame
	void Update () {

		if(Input.GetKeyDown(KeyCode.R) || Input.GetMouseButtonDown(1)) // Resolver
        {
            if (_solved) return;

			StopAllCoroutines();

			if (getLast (_createdMatrix)) {

				string result = RecuSolve (_createdMatrix.Clone (), 0, 0, 0, solution) ?
				"Succes" : "Fail";
				if (result == "Succes")
					FillSudoku ();
			}
            else
            {
				feedback.text = CheckValidity(_createdMatrix.Clone()) ? "VALID" : "INVALID";
			}
		}
		else if(Input.GetKeyDown(KeyCode.C) || Input.GetMouseButtonDown(0)) // Validar
        {
            if (_solved) return;

            StopAllCoroutines();

			ClearBoard();

			var matrix = new Matrix<int>(_bigSide, _bigSide);
			SetBoardFromIntMatrix (matrix, false);

			feedback.text = CheckValidity(matrix.Clone()) ? "VALID" : "INVALID";
		}
	
	}

}
