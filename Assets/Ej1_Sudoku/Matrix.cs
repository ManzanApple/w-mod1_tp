﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Matrix<T> : IEnumerable<T>
{
	public int Width { get; set; }
	public int Height { get; set; }

	private T[,] _currentMatrix;


	public Matrix(int width, int height)
    {
        Width = width;
        Height = height;

        _currentMatrix = new T[width, height];
	}

    public Matrix(T[,] original)
    {
        Width = original.GetLength(1);
        Height = original.GetLength(0);

        _currentMatrix = original;
    }

    public Matrix<T> Clone()
    {
        Matrix<T> cloned = new Matrix<T>(Width, Height);

        for (int x = 0; x < Width; x++)
        {
            for (int y = 0; y < Height; y++)
            {
                cloned[x, y] = _currentMatrix[x, y];
            }
        }

        return cloned;
	}

	//Todos los parametros son INCLUYENTES
	public void SetRangeTo(int x0, int y0, int x1, int y1, T item)
    {
        for (int col = x0; col < x1; col++)
        {
            for (int row = y0; row < y1; row++)
            {
                _currentMatrix[col, row] = item;
            }
        }
    }

	//Todos los parametros son INCLUYENTES
	public List<T> GetRange(int x0, int y0, int x1, int y1)
    {
        List<T> returnRange = new List<T>();

        for (int col = x0; col <= x1; col++)
        {
            for (int row = y0; row <= y1; row++)
            {
                returnRange.Add(_currentMatrix[col, row]);
            }
        }

        return returnRange;
	}

	public T this[int x, int y]
    {
		get
        {
			return _currentMatrix[x, y];
		}
		set
        {

			_currentMatrix[x, y] = value;

		}
	}

	public IEnumerator<T> GetEnumerator()
    {
        for (int col = 0; col < Width; col++)
        {
            for (int row = 0; row < Height; row++)
            {
                yield return _currentMatrix[col, row];
            }
        }
    }

	IEnumerator IEnumerable.GetEnumerator()
    {
		return GetEnumerator();
	}


}
