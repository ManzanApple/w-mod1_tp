﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

class Utility {

	//Intente que esta función tenga una complejidad algorítmica menor a O(n^2) para el peor caso (o sea, que sea mas eficiente que una cuadrática)
	//No utilize la funcion Distinct de LINQ.
	public static bool Unique(IEnumerable<int> numbers) {
		//...IMPLEMENTAR...
		return false;
	}

	public static Direction Opposite(Direction dir) {
		//...IMPLEMENTAR...
		throw new System.Exception("Invalid direction, has no opposite");
	}

	//Extrae un valor random de la lista (Y lo remueve)
	public static T ExtractRandom<T>(List<T> list) {
		//...IMPLEMENTAR...
		return default(T);
	}

}

